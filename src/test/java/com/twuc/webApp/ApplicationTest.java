package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {


    @Test
    void should_return_ticket_when_saving_bag() {
//        given
        Storage storage = new Storage();
        Bag bag = new Bag();
//        when then
        Ticket ticket = storage.save(bag);

        assertNotNull(ticket);
    }

    @Test
    void should_return_ticket_when_saving_nothing() {
        Storage storage = new Storage();
        Bag noting = null;
        Ticket ticket = storage.save(noting);
        assertNotNull(ticket);
    }

    @Test
    void should_return_saved_correct_bag_when_use_valid_ticket_retrieving_bag() {
        Storage storage = new Storage();
        Bag oneBag = new Bag();
        Ticket oneTicket = storage.save(oneBag);

        Bag anotherBag = new Bag();
        Ticket anotherTicket = storage.save(anotherBag);

        assertSame(oneBag,storage.retrieving(oneTicket));
        assertSame(anotherBag,storage.retrieving(anotherTicket));
    }

    @Test
    void should_return_error_message_when_use_invalid_ticket_retrieving_bag() {
        Storage storage = new Storage();
        Bag bag = new Bag();
        Ticket invalidTicket = storage.save(bag);
        Bag firstRetrieve = storage.retrieving(invalidTicket);
        IllegalArgumentException errorMessage = assertThrows(IllegalArgumentException.class, () -> storage.retrieving(invalidTicket));
    }

    @Test
    void should_return_nothing_when_use_valid_ticket_retrieving_nothing() {
        Storage storage = new Storage();
        Bag nothing = null;
        Ticket saveNothingTicket = storage.save(nothing);

        Bag getNothing = storage.retrieving(saveNothingTicket);
        assertNull(getNothing);
    }

    @Test
    void should_return_ticket_when_storage_capacity_is_2_and_saving_bag() {
        int capacity = 2;
        Storage storage = new Storage(capacity);
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_return_message_when_storage_capacity_is_full_and_saving_bag() {
        int capacity = 2;
        Storage storage = new Storage(capacity);
        storage.save(new Bag());
        storage.save(new Bag());

        Bag savebag = new Bag();
        String message = assertThrows(RuntimeException.class, () -> storage.save(savebag)).getMessage();
        assertEquals("can't save",message);
    }

}