package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private Map<Ticket,Bag> storage;
    private int capacity;

    public Storage(int capacity) {
        this.storage = new HashMap<>(capacity);
        this.capacity = capacity;
    }

    public Storage() {
        this.storage = new HashMap<>();
    }

    public Ticket save(Bag bag) {
        if (storage.size()==capacity){
            throw new RuntimeException("can't save");
        }
        Ticket ticket = new Ticket();
        storage.put(ticket,bag);
        return ticket;
    }
    public Bag retrieving(Ticket ticket){
        if (!storage.containsKey(ticket)){
            throw new IllegalArgumentException("Invalid Ticket");
        }
        Bag bag = storage.get(ticket);
        storage.remove(ticket);
        return bag;
    }
}
